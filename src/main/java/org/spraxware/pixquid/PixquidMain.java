package org.spraxware.pixquid;

import com.google.common.collect.Sets;
import org.spraxware.pixquid.grid.Cell;
import org.spraxware.pixquid.grid.Grid;
import processing.core.PApplet;

import java.awt.*;
import java.util.Set;


public class PixquidMain extends PApplet {

    public static void main(String[] args) {
        PApplet.main(PixquidMain.class);
    }

    private static final boolean DEV_MODE = false;

    private static final int WIDTH = 500;
    private static final int HEIGHT = 500;

    private static final int CELL_SIZE = 25;

    private static final float X_OFFSET = CELL_SIZE / 4;
    private static final float y_OFFSET_XY = CELL_SIZE * 0.25f;
    private static final float Y_OFFSET_CV = CELL_SIZE * 0.5f;
    private static final float Y_OFFSET_IF = CELL_SIZE * 0.75f;

    private LiquidSim liquidSim;
    private Grid grid;
    private Set<Cell> allCells;

    public void settings() {
        size(WIDTH, HEIGHT);
    }

    public void setup() {
        rectMode(CORNERS);
        frameRate(30);
        noStroke();

        grid = Grid.create(WIDTH / CELL_SIZE, HEIGHT / CELL_SIZE);
        liquidSim = LiquidSim.create(grid);
        allCells = Sets.newHashSet();

        for (int i = grid.getDepth() - 1; i >= 0; i--) {
            for (int j = grid.getBreadth() - 1; j >= 0; j--) {
                allCells.add(grid.getCell(j,i));
                if (i == 0 || i == grid.getDepth() - 1 || j == 0 || j == grid.getBreadth() - 1) {
                    Cell c = grid.getCell(j, i);
                    c.setBlocked(true);
                }
            }
        }

        if (DEV_MODE) {
            textSize(8);
            frameRate(5);
            strokeWeight(1f);
        }
    }

    public void draw() {
        background(255f);
        checkMouse();

//        simBottomUp();
        simHighPressureFirst();

        for (int i = 0; i < grid.getBreadth(); i++) {
            for (int j = 0; j < grid.getDepth(); j++) {

                Cell cell = grid.getCell(j, i);
                Point location = cell.getGridLocation();

                if (cell.isBlocked()) {
                    cell.setFalling(false);
                    fill(0, 0, 0);

                    rect(cell.getPixelX(CELL_SIZE), cell.getPixelY(CELL_SIZE),
                            cell.getPixelX(CELL_SIZE) + CELL_SIZE, cell.getPixelY(CELL_SIZE) + CELL_SIZE
                    );

                } else {
                    Cell above = grid.getCell(j, i - 1);
                    if (above.getCurrentVolume() > 2 * Cell.MIN_CAPACITY) {
                        cell.setFalling(true);
                        drawFallingLiquid(cell, above);
                    } else {
                        cell.setFalling(false);
                        if (cell.getCurrentVolume() >= 2 * Cell.MIN_CAPACITY) {
                            drawStaticLiquid(cell);
                        }
                    }
                }

                if (DEV_MODE) {
                    fill(cell.isBlocked() ? 255 : 0);
                    text("xy: " + location.x + ", " + location.y, cell.getPixelX(CELL_SIZE) + X_OFFSET, cell.getPixelY(CELL_SIZE) + y_OFFSET_XY);
                    text("cV: " + cell.getCurrentVolume(), cell.getPixelX(CELL_SIZE) + X_OFFSET, cell.getPixelY(CELL_SIZE) + Y_OFFSET_CV);
                    text("iF: " + (cell.isFalling() ? "T" : "F"), cell.getPixelX(CELL_SIZE) + X_OFFSET, cell.getPixelY(CELL_SIZE) + Y_OFFSET_IF);
                }
            }
        }
    }

    private void drawFallingLiquid(Cell cell, Cell above) {
        fill(0,
                0,
                255 - Math.max(128, 32 * (cell.getCurrentVolume() - Cell.MAX_CAPACITY)),
                255 * Math.max(cell.getCurrentVolume(), above.getCurrentVolume())
        );

        rect(cell.getPixelX(CELL_SIZE), cell.getPixelY(CELL_SIZE),
                cell.getPixelX(CELL_SIZE) + CELL_SIZE, cell.getPixelY(CELL_SIZE) + CELL_SIZE
        );
    }

    private void drawStaticLiquid(Cell cell) {
        float mod = Math.max(0, CELL_SIZE - (cell.getCurrentVolume() * CELL_SIZE));
        fill(0,
                0,
                255 - Math.max(128, 32 * (cell.getCurrentVolume() - Cell.MAX_CAPACITY)),
                255 * cell.getCurrentVolume() + 64
        );

        rect(cell.getPixelX(CELL_SIZE), cell.getPixelY(CELL_SIZE) + mod,
                cell.getPixelX(CELL_SIZE) + CELL_SIZE, cell.getPixelY(CELL_SIZE) + CELL_SIZE
        );
    }

    private void simHighPressureFirst() {
        allCells.stream()
                .filter(cell -> !cell.isBlocked())
                .sorted((a, b) -> Float.compare(a.getCurrentVolume(), b.getCurrentVolume()))
                .forEachOrdered(cell -> {
                    liquidSim.flowDown(cell);
                    liquidSim.flowSides(cell);
                    liquidSim.flowUp(cell);
                });
    }

    private void simBottomUp() {
        for (int i = grid.getDepth() - 1; i >= 0; i--) {
            for (int j = grid.getBreadth() - 1; j >= 0; j--) {
                Cell cell = grid.getCell(j, i);
                if (!cell.isBlocked()) {
                    liquidSim.flowDown(cell);
                    liquidSim.flowSides(cell);
                    liquidSim.flowUp(cell);
                }
            }
        }
    }

    public void checkMouse(){
        if (mousePressed) {
            Cell clickedCell = grid.getCell(mouseX / CELL_SIZE, mouseY / CELL_SIZE);
            if (mouseButton == LEFT) {
                clickedCell.setBlocked(true);
            } else if (mouseButton == RIGHT) {
                if (clickedCell.isBlocked()) {
                    clickedCell.setBlocked(false);
                } else {
                    clickedCell.addLiquid(3.0f);
                    // add more
                }
            }
        }
    }
}
