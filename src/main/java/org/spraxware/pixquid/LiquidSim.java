package org.spraxware.pixquid;

import org.spraxware.pixquid.grid.Cell;
import org.spraxware.pixquid.grid.Grid;
// do all for bug for bug then fix bug
public class LiquidSim {

    private final Grid grid;

    public static LiquidSim create(Grid grid) {
        return new LiquidSim(grid);
    }

    private LiquidSim(Grid grid) {
        this.grid = grid;
    }

    /**
     * If the cell below is blocked then move on. Otherwise flow to the below cell with as much as possible.
     * Returns a bool so we can fill out the block on falling blocks
     * @param cell
     */
    public void flowDown(Cell cell) {
        Cell destination = grid.getCell(cell.getGridLocation().x, cell.getGridLocation().y + 1);

        // cannot flow through blocked cell
        if (destination.isBlocked()) {
            return;
        }
        // destination has space for some liquid
        if (destination.getCurrentVolume() < Cell.MAX_CAPACITY) {
            float space = Cell.MAX_CAPACITY - destination.getCurrentVolume();
            if (space >= cell.getCurrentVolume()) {
                flowLiquid(cell, destination, cell.getCurrentVolume());
            } else {
                flowLiquid(cell, destination, space);
            }
        }
    }

    /**
     * Have to calculate both before doing it so the values are the same for each calculation
     * @param cell
     */
    public void flowSides(Cell cell) {
        Cell left = grid.getCell(cell.getGridLocation().x - 1, cell.getGridLocation().y);
        Cell right = grid.getCell(cell.getGridLocation().x + 1, cell.getGridLocation().y);

        float maxLeftFlow = 0f;
        float maxRightFlow = 0f;

        if (!left.isBlocked() && left.getCurrentVolume() < cell.getCurrentVolume()) {
           float leftGap = cell.getCurrentVolume() - left.getCurrentVolume();
           maxLeftFlow = leftGap / 2f; // make 1.5 for bug
        }

        if (!right.isBlocked() && right.getCurrentVolume() < cell.getCurrentVolume()) {
            float rightGap = cell.getCurrentVolume() - right.getCurrentVolume();
            maxRightFlow = rightGap / 2f; // make 1.5 for bug
        }

        if (maxLeftFlow == 0) {
            flowLiquid(cell, right, maxRightFlow);
            return;
        }

        if (maxRightFlow == 0) {
            flowLiquid(cell, left, maxLeftFlow);
            return;
        }

        float leftRatio = maxLeftFlow / (maxLeftFlow + maxRightFlow);
        float rightRatio = 1.0f - leftRatio;

        flowLiquid(cell, left, leftRatio * maxLeftFlow);
        flowLiquid(cell, right, rightRatio * maxRightFlow);
    }

    /**
     * If a cell ends attempts to flow down, left, and right and is still over capacity, it will start moving upwards
     * @param cell
     */
    public void flowUp(Cell cell) {
        if (cell.getCurrentVolume() <= Cell.MAX_CAPACITY) {
            return;
        }

        Cell destination = grid.getCell(cell.getGridLocation().x, cell.getGridLocation().y - 1);

        if (destination.isBlocked()) {
            return;
        }

        if (destination.getCurrentVolume() < cell.getCurrentVolume()) {
            float gap = cell.getCurrentVolume() - destination.getCurrentVolume();
            flowLiquid(cell, destination, gap / 4); //make 2 for bug
        }
    }

    private void flowLiquid(Cell current, Cell destination, float liquidVal) {
        if (liquidVal <= 0f) {
            return;
        }
        if (!destination.isBlocked()) {
            destination.addLiquid(liquidVal);
            current.removeLiquid(liquidVal);
        }
    }
}
