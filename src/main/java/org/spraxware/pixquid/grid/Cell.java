package org.spraxware.pixquid.grid;

import java.awt.*;

public class Cell {

    // The current volume is not actually bound by this value, but this is the maximum capacity before the cell
    // would start to build pressure
    public static final float MAX_CAPACITY = 1f;

    // I think this stops some weird floating point bug
    public static final float MIN_CAPACITY = 0.05f;
    private final Point gridLocation;

    private boolean isBlocked;
    private float currentVolume;
    private boolean isFalling;
    private boolean isSpreading;
    private boolean spreadRight;

    public static Cell create(Point gridLocation) {
        return new Cell(gridLocation, MIN_CAPACITY, false);
    }

    public static Cell create(Point gridLocation, float currentVolume, int cellSize) {
        return new Cell(gridLocation, currentVolume, false);
    }

    public static Cell createBlock(Point gridLocation, int cellSize) {
        return new Cell(gridLocation, MIN_CAPACITY, true);
    }

    private Cell(Point gridLocation, float currentVolume, boolean isBlocked) {
        this.gridLocation = gridLocation;
        this.currentVolume = currentVolume;
        this.isBlocked = isBlocked;
        this.isSpreading = false;
    }

    public Point getGridLocation() {
        return gridLocation;
    }

    // pixel locations are dependant on the size of a cell
    public int getPixelX(int cellSize) {
        return gridLocation.x * cellSize;
    }

    public int getPixelY(int cellSize) {
        return gridLocation.y * cellSize;
    }

    public float getCurrentVolume() {
        if (currentVolume <= MIN_CAPACITY) {
            currentVolume = MIN_CAPACITY;
        }
        return currentVolume - MIN_CAPACITY;
    }

    public void addLiquid(float val) {
        if (val < 0) System.out.println("Adding negative liquid!?");
        this.currentVolume += val;
    }

    /**
     * @param val
     */
    public void removeLiquid(float val) {
        if (val < 0) System.out.println("Removing negative liquid!?");
        this.currentVolume -= val;

        // There's some float precision bug somewhere which means this ends up as -0.0000000001 or something stupid
        // so this just guards against that and stops what happens with really slow flow rates
        if (this.isEmpty()) {
            currentVolume = MIN_CAPACITY;
            isFalling = false;
        }
    }

    /**
     * A blocked cell is ignored for simulation
     */
    public boolean isBlocked() {
        return isBlocked;
    }

    public void toggleBlocked() {
        this.isBlocked = !isBlocked;

        if (isBlocked) {
            currentVolume = MIN_CAPACITY;
        }
    }

    public void setBlocked(boolean blocked) {
        this.isBlocked = blocked;
    }

    public void setFalling(boolean falling) {
        this.isFalling = falling;
    }

    public boolean isFalling() {
        return isFalling;
    }

    public boolean isSpreading() {
        return isSpreading;
    }

    public void setSpreading(boolean isSpreading, boolean spreadRight) {
        this.isSpreading = isSpreading;
        this.spreadRight = spreadRight;
    }

    public boolean isSpreadRight() {
        return spreadRight;
    }

    public boolean isEmpty() {
        return currentVolume <= Cell.MIN_CAPACITY;
    }
}
