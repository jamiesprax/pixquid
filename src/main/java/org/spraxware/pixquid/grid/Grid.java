package org.spraxware.pixquid.grid;

import java.awt.*;

public class Grid {

    private final int breadth;
    private final int depth;

    private Cell[][] internalGrid;

    private Grid(int width, int height) {
        this.internalGrid = new Cell[height][width];
        this.breadth = width;
        this.depth = height;

        for (int i = 0; i < depth; i++) {
            for (int j = 0; j < breadth; j++) {
                internalGrid[i][j] = Cell.create(new Point(j, i));
            }
        }

    }

    public static Grid create(int width, int height) {
        return new Grid(width, height);
    }

    public Cell getCell(int x, int y) {
        return internalGrid[y][x];
    }

    public void setCell(Cell cell) {
        internalGrid[cell.getGridLocation().y][cell.getGridLocation().x] = cell;
    }

    public int getBreadth() {
        return breadth;
    }

    public int getDepth() {
        return depth;
    }
}
